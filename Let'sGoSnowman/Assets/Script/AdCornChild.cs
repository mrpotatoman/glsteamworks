﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdCornChild : MonoBehaviour
{

    public bool oneTimeFinishCorn = true;

    public float Adangle = 0f;

    public GameObject cam;
    public GameObject text;

    [Header("傾きの滑らかさ(0.1なら10分の1ずつ傾く)")]
    public float Adsmooth = 0.2f;

    [Header("ゲームオーバーになる条件の角度(左限と右限)")]
    public float AdleftLimit = 0.22f;
    public float AdrightLimit = -0.22f;


    void Update()
    {
        //目的の角度までの数分の1ずつ角度を変える
        Vector3 axis = new Vector3(0f, 0f, 1f);
        Quaternion q = Quaternion.AngleAxis(Adangle, axis);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, Adsmooth);

        //傾きによるゲームオーバー条件
        if (transform.rotation.z >= AdleftLimit || transform.rotation.z <= AdrightLimit)
        {
            if (oneTimeFinishCorn == true)
            {
                cam.GetComponent<AdCam>().AdSlowDown();
                cam.GetComponent<AdCam>().AdStart = false;                         //ゲーム終了に伴うフラグの状態変化
                text.gameObject.SetActive(true);
                oneTimeFinishCorn = false;
            }
        }
    }
}
