﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceUgoku : MonoBehaviour
{
    //このスクリプトは"Prefab"の"IceChiisakunaru"にアタッチされています

    //このスクリプトで行っていること
    //カメラとの距離を測ってゲームオーバー判定(仮)
    //ランダムでフレーバー変更、回転

    //テクスチャのグルーピング案
    //グループごとにメソッドを分けてランダム抽選後にそのメソッドを呼び出す


    GameObject cam;

    int groupNum = 0;

    //以下、フレーバーたち/unity側でぶっこんでいる
    public Material banana;
    public Material blueberry;
    public Material chocolate;
    public Material chocolate2;
    public Material chocolate3;
    public Material chocolate4;
    public Material cookieandcream;
    public Material mysterious;
    public Material halloween;
    public Material orange;
    public Material soda;
    public Material soda2;
    public Material grape;
    public Material strawberry;
    public Material strawberry2;
    public Material strawberry3;
    public Material strawberry4;
    public Material vanilla;
    public Material blue1;
    public Material blue2;
    public Material blue3;
    public Material whitetiramisu;
    public Material wintershortcake;
    public Material icecreamhunt;
    public Material azuki;
    public Material bananaandstrawberry;
    public Material chocolatemint;
    public Material meltystollen;
    public Material thirtyparty;
    public Material greentea;
    public Material greenstar;
    public Material tatami;
    public Material xmas;
    public Material milkyway;
    public Material tanabata;

    void Start()
    {
        cam = GameObject.Find("MainCamera");

        groupNum = Global.randomNum;
        SelectMaterial();
    }

    void Update()
    {
        //アイスとカメラの座標を取得
        Vector3 posi1 = transform.position;
        Vector3 posi2 = cam.transform.position;
        //ベクトルの引き算をして結果を代入
        Vector3 d1 = posi2 - posi1;
        float dm = d1.magnitude;


        //親の落下フラグを見て、落下中だったら
        if (transform.parent.gameObject.GetComponent<IceParent>().isColli == false)
        {
            //落下中にランダム速度でぐるぐる回ってテクスチャが回転しているかのように見せる
            if (dm > 23.5f)
            {
                float zAnglRand = Random.Range(0, 361);                                     //ランダムで速度を決定

                transform.localEulerAngles = new Vector3(0f, 0f, zAnglRand);                //カメラと近づくまで回り続ける
            }
        }
    }

    void SelectMaterial()
    {
        if (groupNum == 0)
        {
            int num = Random.Range(0, 7);
            switch (num)
            {
                case 0:
                    GetComponent<Renderer>().material = blue1;
                    break;
                case 1:
                    GetComponent<Renderer>().material = blue2;
                    break;
                case 2:
                    GetComponent<Renderer>().material = blue3;
                    break;
                case 3:
                    GetComponent<Renderer>().material = thirtyparty;
                    break;
                case 4:
                    GetComponent<Renderer>().material = soda2;
                    break;
                case 5:
                    GetComponent<Renderer>().material = milkyway;
                    break;
                case 6:
                    GetComponent<Renderer>().material = tanabata;
                    break;
            }
        }

        if (groupNum == 1)
        {
            int num = Random.Range(0, 7);
            switch (num)
            {
                case 0:
                    GetComponent<Renderer>().material = chocolate;
                    break;
                case 1:
                    GetComponent<Renderer>().material = chocolate2;
                    break;
                case 2:
                    GetComponent<Renderer>().material = chocolate3;
                    break;
                case 3:
                    GetComponent<Renderer>().material = chocolate4;
                    break;
                case 4:
                    GetComponent<Renderer>().material = halloween;
                    break;
                case 5:
                    GetComponent<Renderer>().material = orange;
                    break;
                case 6:
                    GetComponent<Renderer>().material = cookieandcream;
                    break;
            }
        }

        if (groupNum == 2)
        {
            int num = Random.Range(0, 7);
            switch (num)
            {
                case 0:
                    GetComponent<Renderer>().material = azuki;
                    break;
                case 1:
                    GetComponent<Renderer>().material = blueberry;
                    break;
                case 2:
                    GetComponent<Renderer>().material = strawberry;
                    break;
                case 3:
                    GetComponent<Renderer>().material = strawberry2;
                    break;
                case 4:
                    GetComponent<Renderer>().material = strawberry3;
                    break;
                case 5:
                    GetComponent<Renderer>().material = strawberry4;
                    break;
                case 6:
                    GetComponent<Renderer>().material = grape;
                    break;
            }
        }

        if (groupNum == 3)
        {
            int num = Random.Range(0, 7);
            switch (num)
            {
                case 0:
                    GetComponent<Renderer>().material = banana;
                    break;
                case 1:
                    GetComponent<Renderer>().material = meltystollen;
                    break;
                case 2:
                    GetComponent<Renderer>().material = vanilla;
                    break;
                case 3:
                    GetComponent<Renderer>().material = whitetiramisu;
                    break;
                case 4:
                    GetComponent<Renderer>().material = bananaandstrawberry;
                    break;
                case 5:
                    GetComponent<Renderer>().material = whitetiramisu;
                    break;
                case 6:
                    GetComponent<Renderer>().material = soda;
                    break;
            }
        }

        if (groupNum == 4)
        {
            int num = Random.Range(0, 7);
            switch (num)
            {
                case 0:
                    GetComponent<Renderer>().material = chocolatemint;
                    break;
                case 1:
                    GetComponent<Renderer>().material = icecreamhunt;
                    break;
                case 2:
                    GetComponent<Renderer>().material = mysterious;
                    break;
                case 3:
                    GetComponent<Renderer>().material = greentea;
                    break;
                case 4:
                    GetComponent<Renderer>().material = greenstar;
                    break;
                case 5:
                    GetComponent<Renderer>().material = tatami;
                    break;
                case 6:
                    GetComponent<Renderer>().material = xmas;
                    break;
            }
        }
    }
}

