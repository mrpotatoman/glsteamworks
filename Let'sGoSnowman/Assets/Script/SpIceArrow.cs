﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpIceArrow : MonoBehaviour
{
    private GameObject cam;
    private GameObject resScoreText;
    private GameObject resText;
    private GameObject corn;
    //private GameObject director;
    //private GameObject dj;
    //private GameObject sbPanel;
    //private GameObject fin;
    //private GameObject best;

    public GameObject spEffectLight;
    public GameObject spEffectSpark;
    public GameObject spEffectStar;
    private bool one = true;

    private bool spIsColli = false;

    private bool onlyOneTimeColli = true;
    //private bool oneTimeFin = true;
    //private bool oneTimeSlow = true;

    [Header("こいつの落下速度")]
    public float spYaxis = -7f;
    //[Header("スローになる距離")]
    //public float beSlowDistance = 23f;
    //[Header("ゲームオーバーになる距離")]
    //public float gameoverDistance = 23.5f;

    void Start()
    {
        cam = GameObject.Find("MainCamera");
        resScoreText = GameObject.Find("ResultScoreText");
        resText = GameObject.Find("ResultText");
        corn = GameObject.Find("icecreamcorn");
        //dj = GameObject.Find("DJ");
        corn = GameObject.Find("icecreamcorn");
        //sbPanel = GameObject.Find("ScoreBackPanel");
        //fin = GameObject.Find("Finish");
        //best = GameObject.Find("BestScore");
        //director = GameObject.Find("IceDirector");
    }

    void Update()
    {
        //アイス落下の文
        if (spIsColli == false)
        {
            //こんくらい動いてほしい(Yだけ変数)
            this.transform.Translate(0.0f, spYaxis * Time.deltaTime, 0.0f);
            if (one == true)
            {
                spEffectLight.GetComponent<ParticleSystem>().Play();        //落下中はエフェクト再生
                spEffectSpark.GetComponent<ParticleSystem>().Play();
            }

            if (resText.GetComponent<RetryText>().gameover == true)         //ゲームオーバー時
            {
                spEffectLight.GetComponent<ParticleSystem>().Stop();
                spEffectSpark.GetComponent<ParticleSystem>().Stop();
                Destroy(gameObject);
            }
        }
        else if (spIsColli == true)
        {
            spYaxis = 0f;
            spEffectLight.GetComponent<ParticleSystem>().Stop();            //接触時はエフェクト停止
            spEffectSpark.GetComponent<ParticleSystem>().Stop();
        }


        //カメラから離れたらゲームオーバー
        //if (spIsColli == false)
        //{
        //    //アイスとカメラの座標を取得
        //    Vector3 posi1 = transform.position;
        //    Vector3 posi2 = cam.transform.position;
        //    //ベクトルの引き算をして結果を代入
        //    Vector3 d1 = posi2 - posi1;
        //    float dm = d1.magnitude;

        //    //もし距離が一定以上離れていたら
        //    if (dm > beSlowDistance)
        //    {
        //        if (oneTimeSlow == true)
        //        {
        //            cam.GetComponent<CameraAction>().SlowDown();
        //            oneTimeSlow = false;
        //        }
        //        if (dm > gameoverDistance)
        //        {
        //            if (oneTimeFin == true)
        //            {
        //                //リザルトを表示
        //                //アイスを消してアイス落下フラグをfalseに変える 
        //                resScoreText.GetComponent<Text>().color -= new Color(0f, 0f, 0f, 255f);     //スコアを非表示
        //                sbPanel.gameObject.SetActive(false);                                        //帯を非表示
        //                best.gameObject.SetActive(false);                                           //ハイスコア非表示
        //                fin.GetComponent<Text>().color += new Color(0f, 0f, 0f, 255f);              //Finish表示
        //                dj.GetComponent<AudioSource>().Stop();                                      //BGM停止
        //                resScoreText.GetComponent<AudioSource>().Play();                            //効果音再生
        //                director.GetComponent<IceDirector>().start = false;                         //ゲーム終了に伴うフラグの状態変化
        //                resText.GetComponent<RetryText>().gameover = true;                          //ゲーム終了に伴うフラグの状態変化
        //                fin.GetComponent<FinishScript>().readyDisp = true;                          //次のテキスト表示準備
        //                oneTimeFin = false;
        //            }
        //        }
        //    }
        //}
    }

    void OnCollisionEnter(Collision other)
    {
        if (onlyOneTimeColli == true)
        {
            //カメラに付けた効果音を再生
            cam.GetComponent<AudioSource>().Play();

            //エフェクト再生
            spEffectStar.GetComponent<ParticleSystem>().Play();

            //スコア加算
            if (ScoreScript.isDoubleScore == false)
            {
                resScoreText.GetComponent<ScoreScript>().scoreHowManyIce += 10;
            }
            else if (ScoreScript.isDoubleScore == true)
            {
                resScoreText.GetComponent<ScoreScript>().scoreHowManyIce += 20;
            }

            //コーンの傾きに影響を与える
            //コーンとの距離を2倍して傾きの値にする
            corn.GetComponent<CornAction>().angle -= (transform.position.x - corn.transform.position.x) * 2f;

            //接触しているかを知らせる
            spIsColli = true;

            //もし最高到達点を更新していたらカメラの移動目標値を変更
            if (transform.position.y - 0.3f > cam.transform.position.y)
            {
                cam.GetComponent<CameraAction>().targetPosition = transform.position + new Vector3(-transform.position.x, -0.3f, -17.53f);
            }

            //アイスが当たったやつの子供になる(厳密にはそいつを親だと認識する)
            transform.parent = other.gameObject.transform;

            onlyOneTimeColli = false;
        }
    }
}
