﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Global : MonoBehaviour
{
    //何にもアタッチされていないが、重要なことはここで行っています
    //ほとんどがカスタムイベントの送信用メソッド
    
    public static bool isFirst = true;                      //起動後初回プレイかどうかを判別するフラグ
    public static int playCounter = 0;                      //何回プレイしたか
    public static int logInCounter = 0;                     //起動回数を記録
    public static string logInCountKey = "LOGINTIMES";      //起動回数保存用
    public static int logInDaysCounter = 1;                 //ログイン日数を記録
    public static string logInDaysCountKey = "LOGINDAYS";   //ログイン日数保存用
    public static int totalPlayCount = 0;                 //累計リプレイ回数を記録
    public static string playCountKey = "HOWMANYRETRY";     //累計リプレイ回数保存用
    public static float stayTime;                           //アプリ滞留時間計測用
    public static string stayTimeKey = "HOWLONG";           //アプリ滞留時間保存用
    public static int showVideoCounter = 0;                 //動画広告表示回数カウント用

    public static int newLoginBonus = 1;                       //ログボ用に新規作成
    public static string newLoginBonusKey = "NEWLOGINBONUS";      //ログボ保存用
    public static bool login2Days = false;                  //2日ログインしていればtrueになる
    public static bool login3Days = false;
    public static bool login5Days = false;
    public static bool login7Days = false;
    public static int randomNum = 0;
    public static bool dispLogb2 = false;
    public static bool dispLogb3 = false;
    public static bool dispLogb5 = false;
    public static bool dispLogb7 = false;
    public static bool isFirstResult = true;                //ログイン促進用



    //起動回数加算+報告
    public static void AddLogInTimes()
    {
        logInCounter = PlayerPrefs.GetInt(logInCountKey, 0);

        logInCounter++;

        PlayerPrefs.SetInt(logInCountKey, logInCounter);

        GLS.GLSAnalyticsUtility.TrackEvent("SendTotalLogInTimesReport", "TotalLogInTimes", logInCounter);
    }

    //ログイン日数加算(チェック)+報告
    public static void AddLogInDays()
    {
        logInDaysCounter = PlayerPrefs.GetInt(logInDaysCountKey, 1);
        newLoginBonus = PlayerPrefs.GetInt(newLoginBonusKey, 1);

        DateTime now = DateTime.Now;
        int todayInt = 0;

        todayInt = now.Year * 1000 + now.Month * 100 + now.Day;

        if(!PlayerPrefs.HasKey("Date"))
        {
            Debug.Log("Dateというデータが存在しません");
            PlayerPrefs.SetInt("Date", todayInt);
            GLS.GLSAnalyticsUtility.TrackEvent("RefineLogInDaysReport", "LogInDays", logInDaysCounter);
        }
        else
        {
            if(todayInt - PlayerPrefs.GetInt("Date") > 0)
            {
                PlayerPrefs.SetInt("Date", todayInt);
                Debug.Log("次の日になりました");
                logInDaysCounter++;
                PlayerPrefs.SetInt(logInDaysCountKey, logInDaysCounter);
                newLoginBonus++;
                PlayerPrefs.SetInt(newLoginBonusKey, newLoginBonus);
                GLS.GLSAnalyticsUtility.TrackEvent("RefineLogInDaysReport", "LogInDays", logInDaysCounter);
                if(newLoginBonus == 2)
                {
                    dispLogb2 = true;
                }
                if(newLoginBonus == 3)
                {
                    dispLogb3 = true;
                }
                if(newLoginBonus == 5)
                {
                    dispLogb5 = true;
                }
                if(newLoginBonus == 7)
                {
                    dispLogb7 = true;
                }
            }
            else
            {
                Debug.Log("今日はすでにログインしています");
            }
        }
    }


    //ログボ解禁確認用
    public static void LoginBonusCheck()
    {
        if(newLoginBonus >= 2)
        {
            login2Days = true;

            if(newLoginBonus >= 3)
            {
                login3Days = true;

                if(newLoginBonus >= 5)
                {
                    login5Days = true;

                    if(newLoginBonus >= 7)
                    {
                        login7Days = true;
                    }
                }
            }
        }
        else
        {
            Debug.Log("LOGINDAYS = " + newLoginBonus);
        }
    }

    public static void RandomIceSelect()
    {
        if(login2Days == false)
        {
            randomNum = 0;
        }
        else if(login2Days == true)
        {
            if(login3Days == false)
            {
                Random2();
            }
            else if(login3Days == true)
            {
                if(login5Days == false)
                {
                    Random3();
                }
                else if(login5Days == true)
                {
                    if (login7Days == false)
                    {
                        Random4();
                    }
                    else if(login7Days == true)
                    {
                        Random5();
                    }
                }
            }
        }
        
    }

    public static void Random5()
    {
        randomNum = UnityEngine.Random.Range(0, 5);
    }

    public static void Random4 ()
    {
        randomNum = UnityEngine.Random.Range(0, 4);
    }

    public static void Random3()
    {
        randomNum = UnityEngine.Random.Range(0, 3);
    }

    public static void Random2()
    {
        randomNum = UnityEngine.Random.Range(0, 2);
    }

    //プレイ回数加算+報告
    public static void SendReplayCount()
    {
        //累計
        totalPlayCount = PlayerPrefs.GetInt(playCountKey, 0);
        totalPlayCount++;
        PlayerPrefs.SetInt(playCountKey, totalPlayCount);
        CasebyCase(totalPlayCount);     //一定数ごとにカスタムイベント送信

        //今回
        playCounter++;
        CasebyCase2(playCounter);       //一定数ごとにカスタムイベント送信
    }

    public static void CasebyCase(int value)
    {
        if (value == 5)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 10)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 15)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 20)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 25)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 30)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 40)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 50)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 100)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else if (value == 200)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineTotalPlayCount", "TotalPlay", value);
        }
        else
        {
            return;
        }
    }

    public static void CasebyCase2(int value)
    {
        if (value == 5)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 10)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 15)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 20)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 25)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 30)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 40)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 50)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 75)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else if (value == 100)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefinePlayCount", "PlayCount", value);
        }
        else
        {
            return;
        }
    }
}
