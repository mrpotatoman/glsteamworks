﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdGenerator : MonoBehaviour
{
    public GameObject birdPrefab;
    //時間間隔最小
    public float minspan = 1.0f;
    //時間間隔最大
    public float Maxspan = 4.0f;
    //鳥生成時間間隔
    private float interval;
    //経過時間
    private float delta = 0;

    [SerializeField]
    private GameObject director;
    [SerializeField]
    private GameObject mainCamera;
    void Start()
    {
        interval = RandomTime();
    }

    void Update()
    {
        //ゲーム開始後から生成される
        if (director.GetComponent<IceDirector>().start == true)
        {
            //カメラが生成範囲内に居るなら
            if (mainCamera.transform.position.y < 42f)
            {
                delta += Time.deltaTime;
                
                //生成間隔を超えたら
                if (delta > interval)
                {
                    delta = 0;
                    GameObject bird = Instantiate(birdPrefab);
                    int y = Random.Range(2, 30);
                    bird.transform.position = new Vector3(10, y, 0);
                }
            }
        }
    }

    //ランダム抽選メソッド
　　private float RandomTime()
    {
        return Random.Range(minspan, Maxspan);
    }
}
