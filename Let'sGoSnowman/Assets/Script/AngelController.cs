﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AngelController : MonoBehaviour
{
    //1秒ごとに上下に周期的な移動をさせています
    
    private float delta = 0f;

    private float spantime = 2f;

    private bool upAndDown = true;
    
    void Update()
    {
        delta += Time.deltaTime;

        //上に行けというフラグなら
        if (upAndDown == true)
        {
            transform.Translate(0f, 1f * Time.deltaTime, 0f);       //上へ移動
        }
        //下へ行けというフラグなら
        else if (upAndDown == false)
        {
            transform.Translate(0f, -1f * Time.deltaTime, 0f);      //下へ移動
        }

        //1秒ごとにフラグの入れ替え
        if (delta > spantime)
        {
            delta = 0f;
            if(upAndDown == true)       //上行け状態なら
            {
                upAndDown = false;      //次は下へ
            }
            else　                      //下行け状態なら
            {
                upAndDown = true;       //次は上へ
            }
        }              
    }
}
