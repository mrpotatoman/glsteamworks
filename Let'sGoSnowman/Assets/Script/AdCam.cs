﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdCam : MonoBehaviour
{
    public float AdnewIceTransY = 17f;

    public Vector3 AdtargetPosition;

    //アイスが生成される範囲の幅にあたる部分
    public float AdxWidthLeft = -4.4f;
    public float AdxWidthRight = 4.4f;

    //時間でアイスを生成する
    private bool AdIceCreateTimeFlag = true;  //時間経過でこの旗が立った時にアイスが作られる
    private float AdcreateDelta = 0f;          //空の箱
    private float AdcreateSpan = 1.5f;         //何秒で満杯になったとみなすか

    public GameObject AdparentIcePrefab;

    public bool AdStart = true;

    [Header("スローの時にtimeScaleに設定する時間の進み具合の値")]
    [SerializeField]
    private float AdtimeScale = 0.1f;
    [Header("スローにしたい時間の長さ")]
    [SerializeField]
    private float AdslowTime = 1f;

    private float AdelapsedTime = 0f;
    public bool AdisSlowDown = false;

    void Start()
    {
        AdtargetPosition = transform.position;
    }

    void Update()
    {
        if (AdtargetPosition.y < transform.position.y)
        {
            AdtargetPosition = transform.position;
        }


        //カメラが滑らかに移動する文
        transform.position = (transform.position * 9 + AdtargetPosition) / 10.0f;


        //一定時間でアイス生成の旗を立てる文
        AdcreateDelta += Time.deltaTime;
        if (AdcreateDelta > AdcreateSpan)
        {
            AdcreateDelta = 0f;
            AdIceCreateTimeFlag = true;
        }

        //ゲームが開始されているか
        if (AdStart == true)
        {
            //監督スクリプトの中の落下フラグがfalseなら
            if (AdIceCreateTimeFlag == true)
            {
                var ice = AdIceCreate();

                float newx = Random.Range(AdxWidthLeft, AdxWidthRight);
                float newy = transform.position.y + AdnewIceTransY;
                //作ったオブジェクトをこの場所に移動させる
                ice.transform.position = new Vector3(newx, newy, 0);
                //まずfalseに変える
                AdIceCreateTimeFlag = false;
            }

        }

        if (AdisSlowDown == true)
        {
            AdelapsedTime += Time.unscaledDeltaTime;          //スローの影響を受けないunscaledDeltaTimeを使う
            if (AdelapsedTime >= AdslowTime)
            {
                AdSetNormalTime();
            }
        }
    }

    private GameObject AdIceCreate()
    {
        //iceCreamPrefabって箱に入れたプレハブを生成する
        var newIce = Instantiate(AdparentIcePrefab);     
        return newIce;
    }

    //スローモーション用
    public void AdSlowDown()
    {
        AdelapsedTime = 0f;
        Time.timeScale = AdtimeScale;
        AdisSlowDown = true;
    }

    //通常速度に戻す用
    public void AdSetNormalTime()
    {
        Time.timeScale = 1f;
        AdisSlowDown = false;
    }
}
