﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdIceChild : MonoBehaviour
{
    GameObject cam;

    //以下、フレーバーたち/unity側でぶっこんでいる
    public Material banana;
    public Material blueberry;
    public Material chocolate;
    public Material chocolate2;
    public Material chocolate3;
    public Material chocolate4;
    public Material cookieandcream;
    public Material mysterious;
    public Material halloween;
    public Material orange;
    public Material soda;
    public Material soda2;
    public Material grape;
    public Material strawberry;
    public Material strawberry2;
    public Material strawberry3;
    public Material strawberry4;
    public Material vanilla;
    public Material blue1;
    public Material blue2;
    public Material blue3;
    public Material whitetiramisu;
    public Material wintershortcake;
    public Material icecreamhunt;
    public Material azuki;
    public Material bananaandstrawberry;
    public Material chocolatemint;
    public Material meltystollen;
    public Material thirtyparty;
    public Material greentea;
    public Material greenstar;
    public Material tatami;
    public Material xmas;
    public Material milkyway;
    public Material tanabata;

    void Start()
    {
        RandomMaterial();

        cam = GameObject.Find("MainCamera");
    }

    void Update()
    {
        //アイスとカメラの座標を取得
        Vector3 posi1 = transform.position;
        Vector3 posi2 = cam.transform.position;
        //ベクトルの引き算をして結果を代入
        Vector3 d1 = posi2 - posi1;
        float dm = d1.magnitude;


        //親の落下フラグを見て、落下中だったら
        if (transform.parent.gameObject.GetComponent<AdIceParent>().AdisColli == false)
        {
            //落下中にランダム速度でぐるぐる回ってテクスチャが回転しているかのように見せる
            if (dm > 23f)
            {
                float zAnglRand = Random.Range(0, 361);                                     //ランダムで速度を決定

                transform.localEulerAngles = new Vector3(0f, 0f, zAnglRand);                //カメラと近づくまで回り続ける
            }
        }
    }

    void RandomMaterial()
    {
        //アイスが生まれるときに一度だけ数字が抽選されて
        int num = Random.Range(0, 7);

        //その数字によってテクスチャが変わる
        switch (num)
        {
            case 0:
                GetComponent<Renderer>().material = chocolatemint;
                break;
            case 1:
                GetComponent<Renderer>().material = icecreamhunt;
                break;
            case 2:
                GetComponent<Renderer>().material = mysterious;
                break;
            case 3:
                GetComponent<Renderer>().material = greentea;
                break;
            case 4:
                GetComponent<Renderer>().material = greenstar;
                break;
            case 5:
                GetComponent<Renderer>().material = tatami;
                break;
            case 6:
                GetComponent<Renderer>().material = xmas;
                break;
        }
    }
}
