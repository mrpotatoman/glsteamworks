﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IceDirector : MonoBehaviour
{
    //このスクリプトは"PlayScene"の"IceDirector"にアタッチされています

    //フレームレートの固定
    //カスタムイベントの送信
    //滞留時間計測、ログイン時間確認


    public bool icecol = false;             //アイスがぶつかった時のフラグ


    //時間でアイスを生成する
    public bool IceCreateTimeFlag = false;  //時間経過でこの旗が立った時にアイスが作られる
    public float createDelta = 0f;          //空の箱
    public float createSpan = 1.5f;         //何秒で満杯になったとみなすか


    //ゲーム開始時に操作方法などのUIを削除するためのもの
    public GameObject unActive1;
    public GameObject unActive2;
    public GameObject unActive3;
    public GameObject dj;
    public GameObject title;

    public GameObject logb2;
    public GameObject logb3;
    public GameObject logb5;
    public GameObject logb7;


    public bool start = false;
    private bool one = true;
    private bool oneTimeDisp2 = true;
    private bool oneTimeDisp3 = true;
    private bool oneTimeDisp5 = true;
    private bool oneTimeDisp7 = true;

    //カスタムイベント送信回数制御用フラグ(1度だけ)
    private bool oneTime60sec = true;
    private bool oneTime120sec = true;
    private bool oneTime180sec = true;
    private bool oneTime300sec = true;
    private bool oneTime600sec = true;
    private bool oneTime900sec = true;
    private bool oneTime1200sec = true;

    private bool oneTimeAdvd5 = true;
    private bool oneTimeAdvd10 = true;
    private bool oneTimeAdvd15 = true;
    private bool oneTimeAdvd20 = true;
    private bool oneTimeAdvd25 = true;
    private bool oneTimeAdvd30 = true;
    private bool oneTimeAdvd35 = true;
    private bool oneTimeAdvd40 = true;
    private bool oneTimeAdvd45 = true;
    private bool oneTimeAdvd50 = true;


    //起動時に行われる
    void Awake()
    {
        //フレームレートを30で固定
        Application.targetFrameRate = 30;

        //日付確認 => ログイン日数加算
        Global.AddLogInDays();

        if (Global.isFirst == true)
        {
            if (Global.dispLogb2 == true)
            {
                if (oneTimeDisp2 == true)
                {
                    logb2.gameObject.SetActive(true);
                    title.GetComponent<Title>().isDispLogb = true;
                    oneTimeDisp2 = false;
                }
            }
            if (Global.dispLogb3 == true)
            {
                if (oneTimeDisp3 == true)
                {
                    logb3.gameObject.SetActive(true);
                    title.GetComponent<Title>().isDispLogb = true;
                    oneTimeDisp3 = false;
                }
            }
            if (Global.dispLogb5 == true)
            {
                if (oneTimeDisp5 == true)
                {
                    logb5.gameObject.SetActive(true);
                    title.GetComponent<Title>().isDispLogb = true;
                    oneTimeDisp5 = false;
                }
            }
            if (Global.dispLogb7 == true)
            {
                if (oneTimeDisp7 == true)
                {
                    logb7.gameObject.SetActive(true);
                    title.GetComponent<Title>().isDispLogb = true;
                    oneTimeDisp7 = false;
                }
            }
        }
    }

    void Update()
    {
        //ゲーム開始時(タップ)、またはリプレイ時にUIを削除する
        if(start == true)
        {
            if (one == true)
            {
                unActive1.gameObject.SetActive(false);
                unActive2.gameObject.SetActive(false);
                unActive3.gameObject.SetActive(false);
                dj.GetComponent<BGMSelector>().BGMSelect();
                one = false;
            }
        }


        //一定時間でアイス生成の旗を立てる文
        createDelta += Time.deltaTime;
        if(createDelta > createSpan)
        {
            createDelta = 0f;
            IceCreateTimeFlag = true;
        }


        //アプリ滞留時間計測
        Global.stayTime += Time.deltaTime;

        //60秒超過時
        if(Global.stayTime > 60f)
        {            
            if(oneTime60sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "60sec");
                oneTime60sec = false;
            }
        }
        //120秒超過時
        if (Global.stayTime > 120f)
        {
            if (oneTime120sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "120sec");
                oneTime120sec = false;
            }
        }
        //180秒超過時
        if (Global.stayTime > 180f)
        {
            if(oneTime180sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "180sec");
                oneTime180sec = false;
            }
        }
        //300秒超過時
        if (Global.stayTime > 300f)
        {
            if(oneTime300sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "300sec");
                oneTime300sec = false;
            }
        }
        //600秒超過時
        if (Global.stayTime > 600f)
        {
            if (oneTime600sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "600sec");
                oneTime600sec = false;
            }
        }
        //900秒超過時
        if (Global.stayTime > 900f)
        {
            if (oneTime900sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "900sec");
                oneTime900sec = false;
            }
        }
        //1200秒超過時
        if (Global.stayTime > 1200f)
        {
            if (oneTime1200sec == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineStayTime", "StayTime", "1200sec");
                oneTime1200sec = false;
            }
        }



        //動画広告5回再生報告
        if (Global.showVideoCounter == 5)
        {
            if(oneTimeAdvd5 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd5 = false;
            }            
        }

        //動画広告10回再生報告
        if(Global.showVideoCounter == 10)
        {
            if(oneTimeAdvd10 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd10 = false;
            }
        }

        //動画広告15回再生報告
        if(Global.showVideoCounter == 15)
        {
            if(oneTimeAdvd15 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd15 = false;
            }
        }
        
        //動画広告20回再生報告
        if (Global.showVideoCounter == 20)
        {
            if (oneTimeAdvd20 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd20 = false;
            }
        }

        //動画広告25回再生報告
        if (Global.showVideoCounter == 25)
        {
            if (oneTimeAdvd25 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd25 = false;
            }
        }
        
        //動画広告30回再生報告
        if (Global.showVideoCounter == 30)
        {
            if (oneTimeAdvd30 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd30 = false;
            }
        }

        //動画広告35回再生報告
        if (Global.showVideoCounter == 35)
        {
            if (oneTimeAdvd35 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd35 = false;
            }
        }

        //動画広告40回再生報告
        if (Global.showVideoCounter == 40)
        {
            if (oneTimeAdvd40 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd40 = false;
            }
        }

        //動画広告45回再生報告
        if (Global.showVideoCounter == 45)
        {
            if (oneTimeAdvd45 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd45 = false;
            }
        }

        //動画広告50回再生報告
        if (Global.showVideoCounter == 50)
        {
            if (oneTimeAdvd50 == true)
            {
                GLS.GLSAnalyticsUtility.TrackEvent("RefineShowVideoCounter", "ShowVideoCounter", Global.showVideoCounter);
                oneTimeAdvd50 = false;
            }
        }
    }
}
