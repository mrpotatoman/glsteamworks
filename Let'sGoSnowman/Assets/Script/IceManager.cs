﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceManager : MonoBehaviour
{

    //IceManagerにアタッチされています
    //このスクリプトではアイス生成を行っています

    public GameObject parentIcePrefab;
    public GameObject spIcePrefab;

    public GameObject IceCreate()
    {
        //iceCreamPrefabって箱に入れたプレハブを生成する
        var newIce = Instantiate(parentIcePrefab);
        return newIce;
    }

    public GameObject SpIceCreate()
    {
        var newIce = Instantiate(spIcePrefab);
        newIce.GetComponent<AudioSource>().Play();
        return newIce;
    }
}
