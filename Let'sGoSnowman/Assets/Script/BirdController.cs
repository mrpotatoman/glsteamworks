﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    public float birdSpeed = -2f;

void Update()
    {
        transform.Translate(birdSpeed * Time.deltaTime, 0f, 0f);
        
        //画面外へ行ったら消去
        if (transform.position.x < -10)
        {
            Destroy(gameObject);
        }
    }
}
