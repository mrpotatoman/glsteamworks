﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CornAction : MonoBehaviour
{
    public float cornTilt = 0f;

    public GameObject director;
    public GameObject resScoreText;
    public GameObject resText;
    public GameObject dj;
    public GameObject sbPanel;
    public GameObject fin;
    public GameObject best;
    public GameObject cam;
    public GameObject greenLeft;
    public GameObject greenRight;
    public GameObject RedLeft;
    public GameObject RedRight;
    public GameObject faceLeft;
    public GameObject faceRight;
    public GameObject cryLeft;
    public GameObject cryRight;
    public GameObject iceImageLeft;
    public GameObject iceImageRight;
    public GameObject finish;

    public bool oneTimeFinishCorn = true;

    public float angle = 0f;


    [Header("傾きの滑らかさ(0.1なら10分の1ずつ傾く)")]
    [SerializeField]
    private float smooth = 0.1f;

    [Header("ゲームオーバーになる条件の角度(左限と右限)")]
    [SerializeField]
    private float leftLimit = 0.22f;
    [SerializeField]
    private float rightLimit = -0.22f;

    [Header("矢印や顔を表示する角度")]
    [SerializeField]
    private float leftUI = 0.11f;
    [SerializeField]
    private float rightUI = -0.11f;


    void Update()
    {
        //目的の角度までの数分の1ずつ角度を変える
        Vector3 axis = new Vector3(0f, 0f, 1f);
        Quaternion q = Quaternion.AngleAxis(angle, axis);
        transform.rotation = Quaternion.Slerp(transform.rotation, q, smooth);
        
        //finishが消える時にこいつらが復活しないように
        if (finish.GetComponent<FinishScript>().skip == false)
        {
            if (transform.rotation.z == 0)
            {
                greenLeft.gameObject.SetActive(false);
                RedLeft.gameObject.SetActive(false);
                faceLeft.gameObject.SetActive(false);
                cryLeft.gameObject.SetActive(false);
                greenRight.gameObject.SetActive(false);
                RedRight.gameObject.SetActive(false);
                faceRight.gameObject.SetActive(false);
                cryRight.gameObject.SetActive(false);
                iceImageLeft.gameObject.SetActive(false);
                iceImageRight.gameObject.SetActive(false);
            }
            //緑矢印表示
            if (transform.rotation.z > 0 && transform.rotation.z <= leftUI)
            {
                greenLeft.gameObject.SetActive(true);
                iceImageLeft.gameObject.SetActive(true);
                greenRight.gameObject.SetActive(false);
                RedLeft.gameObject.SetActive(false);
                RedRight.gameObject.SetActive(false);
                faceLeft.gameObject.SetActive(false);
                faceRight.gameObject.SetActive(false);
                cryLeft.gameObject.SetActive(false);
                cryRight.gameObject.SetActive(false);
                iceImageRight.gameObject.SetActive(false);
            }
            if (transform.rotation.z < 0 && transform.rotation.z >= rightUI)
            {
                greenRight.gameObject.SetActive(true);
                iceImageRight.gameObject.SetActive(true);
                greenLeft.gameObject.SetActive(false);
                RedLeft.gameObject.SetActive(false);
                RedRight.gameObject.SetActive(false);
                faceLeft.gameObject.SetActive(false);
                faceRight.gameObject.SetActive(false);
                cryLeft.gameObject.SetActive(false);
                cryRight.gameObject.SetActive(false);
                iceImageLeft.gameObject.SetActive(false);
            }
            //赤矢印表示
            if (transform.rotation.z > leftUI && transform.rotation.z < leftLimit)
            {
                RedLeft.gameObject.SetActive(true);
                faceLeft.gameObject.SetActive(true);
                iceImageLeft.gameObject.SetActive(true);
                RedRight.gameObject.SetActive(false);
                faceRight.gameObject.SetActive(false);
                cryLeft.gameObject.SetActive(false);
                cryRight.gameObject.SetActive(false);
                greenRight.gameObject.SetActive(false);
                iceImageRight.gameObject.SetActive(false);
            }
            if (transform.rotation.z < rightUI && transform.rotation.z > rightLimit)
            {
                RedRight.gameObject.SetActive(true);
                faceRight.gameObject.SetActive(true);
                iceImageRight.gameObject.SetActive(true);
                greenLeft.gameObject.SetActive(false);
                RedLeft.gameObject.SetActive(false);
                faceLeft.gameObject.SetActive(false);
                cryRight.gameObject.SetActive(false);
                greenLeft.gameObject.SetActive(false);
                iceImageLeft.gameObject.SetActive(false);
            }
            //泣き顔表示
            if (transform.rotation.z >= leftLimit)
            {
                cryLeft.gameObject.SetActive(true);
                RedLeft.gameObject.SetActive(true);
                iceImageLeft.gameObject.SetActive(true);
                RedRight.gameObject.SetActive(false);
                faceRight.gameObject.SetActive(false);
                cryRight.gameObject.SetActive(false);
                greenRight.gameObject.SetActive(false);
                iceImageRight.gameObject.SetActive(false);
            }
            if (transform.rotation.z <= rightLimit)
            {
                cryRight.gameObject.SetActive(true);
                RedRight.gameObject.SetActive(true);
                iceImageRight.gameObject.SetActive(true);
                RedLeft.gameObject.SetActive(false);
                faceLeft.gameObject.SetActive(false);
                cryLeft.gameObject.SetActive(false);
                greenLeft.gameObject.SetActive(false);
                iceImageLeft.gameObject.SetActive(false);
            }
        }

        //傾きによるゲームオーバー条件
        if (transform.rotation.z >= leftLimit || transform.rotation.z <= rightLimit)
        {
            if (oneTimeFinishCorn == true)
            {
                cam.GetComponent<CameraAction>().SlowDown();
                resScoreText.GetComponent<Text>().color -= new Color(0f, 0f, 0f, 255f);     //スコアを非表示
                sbPanel.gameObject.SetActive(false);                                        //帯を非表示
                best.gameObject.SetActive(false);                                           //ハイスコア非表示
                fin.GetComponent<Text>().color += new Color(0f, 0f, 0f, 255f);              //Finish表示
                dj.GetComponent<BGMSelector>().BGMStop();                                   //BGM停止
                resScoreText.GetComponent<AudioSource>().Play();                            //効果音再生
                director.GetComponent<IceDirector>().start = false;                         //ゲーム終了に伴うフラグの状態変化
                resText.GetComponent<RetryText>().gameover = true;                          //ゲーム終了に伴うフラグの状態変化
                fin.GetComponent<FinishScript>().readyDisp = true;                          //次のテキスト表示準備
                oneTimeFinishCorn = false;
            }
        }
    }
}