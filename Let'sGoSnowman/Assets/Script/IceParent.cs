﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IceParent : MonoBehaviour
{
    //public IceManager IceManager { get; set; }


    public float Yaxis = -7f;

    private GameObject cam;
    private GameObject resScoreText;
    private GameObject resText;
    //private GameObject director;
    //private GameObject dj;
    //private GameObject sbPanel;
    //private GameObject fin;
    //private GameObject best;
    private GameObject corn;
    public GameObject colFX;


    private bool OnlyFirstTime = true;
    public bool isColli = false;
    private bool onlyOneTimeColli = true;

    //private bool oneTimeFin = true;
    //private bool oneTimeSlowIce = true;

    //[Header("スローになる距離")]
    //public float beSlowDistance = 23f;
    //[Header("ゲームオーバーになる距離")]
    //public float gameoverDistance = 23.5f;
    [Header("くっついてもいい距離")]
    public float collisionDistance = 21f;

    void Start()
    {
        cam = GameObject.Find("MainCamera");        
        resScoreText = GameObject.Find("ResultScoreText");
        resText = GameObject.Find("ResultText");
        corn = GameObject.Find("icecreamcorn");
        //dj = GameObject.Find("DJ");
        //director = GameObject.Find("IceDirector");
        //sbPanel = GameObject.Find("ScoreBackPanel");
        //fin = GameObject.Find("Finish");
        //best = GameObject.Find("BestScore");
    }

    void Update()
    {
        //アイス落下の文
        if (isColli == false)
        {
            //こんくらい動いてほしい(Yだけ変数)
            this.transform.Translate(0.0f, Yaxis * Time.deltaTime, 0.0f);

            if (resText.GetComponent<RetryText>().gameover == true)
            {
                Destroy(gameObject);
            }
        }
        else if (isColli == true)
        {
            Yaxis = 0f;
        }


        //カメラから離れたらゲームオーバー
        //if (isColli == false)
        //{
        //    //アイスとカメラの座標を取得
        //    Vector3 posi1 = transform.position;
        //    Vector3 posi2 = cam.transform.position;
        //    //ベクトルの引き算をして結果を代入
        //    Vector3 d1 = posi2 - posi1;
        //    float dm = d1.magnitude;

        //    //もし距離が一定以上離れていたら
        //    if (dm > beSlowDistance)
        //    {
        //        if (oneTimeSlowIce == true)
        //        {
        //            cam.GetComponent<CameraAction>().SlowDown();
        //            oneTimeSlowIce = false;
        //        }
        //        if (dm > gameoverDistance)
        //        {
        //            if (oneTimeFin == true)
        //            {
        //                //リザルトを表示
        //                //アイスを消してアイス落下フラグをfalseに変える 
        //                resScoreText.GetComponent<Text>().color -= new Color(0f, 0f, 0f, 255f);     //スコアを非表示
        //                sbPanel.gameObject.SetActive(false);                                        //帯を非表示
        //                best.gameObject.SetActive(false);                                           //ハイスコア非表示
        //                fin.GetComponent<Text>().color += new Color(0f, 0f, 0f, 255f);              //Finish表示       
        //                dj.GetComponent<BGMSelector>().BGMStop();
        //                resScoreText.GetComponent<AudioSource>().Play();                            //効果音再生
        //                director.GetComponent<IceDirector>().start = false;                         //ゲーム終了に伴うフラグの状態変化
        //                resText.GetComponent<RetryText>().gameover = true;                          //ゲーム終了に伴うフラグの状態変化
        //                fin.GetComponent<FinishScript>().readyDisp = true;                          //次のテキスト表示準備
        //                oneTimeFin = false;
        //            }
        //        }
        //    }
        //}
    }

    void OnCollisionEnter(Collision other)
    {
        //アイスとカメラの座標を取得
        Vector3 posi1 = transform.position;
        Vector3 posi2 = cam.transform.position;
        //ベクトルの引き算をして結果を代入
        Vector3 d1 = posi2 - posi1;
        float dm = d1.magnitude;
        if (dm < collisionDistance)
        {
            if (onlyOneTimeColli == true)
            {
                //カメラに付けた効果音を再生
                cam.GetComponent<AudioSource>().Play();

                //エフェクト再生
                colFX.GetComponent<ParticleSystem>().Play();

                //スコア加算
                if (ScoreScript.isDoubleScore == false)
                {
                    resScoreText.GetComponent<ScoreScript>().scoreHowManyIce += 1;
                }
                else if (ScoreScript.isDoubleScore == true)
                {
                    resScoreText.GetComponent<ScoreScript>().scoreHowManyIce += 2;
                }

                //コーンの傾きに影響を与える
                corn.GetComponent<CornAction>().angle -= (transform.position.x - corn.transform.position.x) * 0.5f;

                //接触しているかを知らせる
                isColli = true;

                if (OnlyFirstTime == false)
                {
                    //もし最高到達点を更新していたらカメラの移動目標値を変更
                    if (transform.position.y - 0.3f > cam.transform.position.y)
                    {
                        cam.GetComponent<CameraAction>().targetPosition = transform.position + new Vector3(-transform.position.x, -0.3f, -17.53f);
                    }
                }
                else
                {
                    cam.GetComponent<CameraAction>().targetPosition = transform.position + new Vector3(-transform.position.x, -0.3f, -17.53f);

                    OnlyFirstTime = false;
                }

                //アイスが当たったやつの子供になる(厳密にはそいつを親だと認識する)
                transform.parent = other.gameObject.transform;

                onlyOneTimeColli = false;
            }
        }
    }
}
