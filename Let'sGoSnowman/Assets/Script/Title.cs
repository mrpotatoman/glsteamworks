﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Title : MonoBehaviour
{
    //titleImageにアタッチされています

    //Editor側で細かい調整をするための変数
    public float handMoveSpeed = 400f;                 //手の画像が動く速さ
    public float titleSpeed = -300f;            //タイトル画像の落ちてくる速度
    public float falldownLimit = 352f;           //タイトル画像がどの高さで止まるか
    public float effectPlayLimit = 600f;          //エフェクトはどの高さまで再生するか
    public float handImageLeftLimit = -79f;    //手の画像の移動幅の限界(左側)
    public float handImageRightLimit = 269f;    //手の画像の移動幅の限界(右側)

    public GameObject handImage;
    public GameObject director;
    public GameObject dj;
    public GameObject pEffect2;
    public GameObject pEffect5;

    private bool oneTimeFlag = true;
    private bool onlyOneTimeFlag = true;
    public bool isDispLogb = false;

    void Start()
    {
        if (Global.isFirst == false)
        {
            if (onlyOneTimeFlag == true)
            {
                director.GetComponent<IceDirector>().start = true;  //初回プレイ以外は起動時UIを即削除する
                Global.SendReplayCount();       //プレイ回数加算
                onlyOneTimeFlag = false;
            }
        }

        //バナー広告を表示
        GLS.Ad.ShowBanner();
    }


    void Update()
    {
        //画像の座標取得
        RectTransform rectTitle = GetComponent<RectTransform>();

        //この高さより高かったらタイトル文字が落下してくる
        if (rectTitle.localPosition.y > falldownLimit)
        {
            rectTitle.localPosition += new Vector3(0f, titleSpeed * Time.deltaTime, 0f);
            if (onlyOneTimeFlag == true)
            {
                if (isDispLogb == false)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Touch();
                        onlyOneTimeFlag = false;
                    }
                }
            }
        }
        else
        {
            if (onlyOneTimeFlag == true)
            {
                if (isDispLogb == false)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        Touch();
                        onlyOneTimeFlag = false;
                    }
                }
            }
        }

        //この高さより高かったらエフェクトを再生する
        if (rectTitle.localPosition.y > effectPlayLimit)
        {
            pEffect2.GetComponent<ParticleSystem>().Play();
            pEffect5.GetComponent<ParticleSystem>().Play();
        }


        //手の座標取得
        RectTransform rectHand = handImage.GetComponent<RectTransform>();

        //端っこまで行ったら戻ってきて逆側へ行く
        if (rectHand.localPosition.x < handImageLeftLimit)
        {
            handMoveSpeed *= -1;
        }

        if (rectHand.localPosition.x > handImageRightLimit)
        {
            handMoveSpeed *= -1;
        }

        //手の画像が動くスピード
        rectHand.localPosition += new Vector3(handMoveSpeed * Time.deltaTime, 0f, 0f);



        //ログイン回数加算
        if (Global.isFirst == true)
        {
            if (oneTimeFlag == true)
            {
                Global.AddLogInTimes();
                Global.LoginBonusCheck();
                Debug.Log(Global.newLoginBonus);
                oneTimeFlag = false;
            }
        }
    }

    //タッチされたら
    private void Touch()
    {
        //ゲームスタートフラグを立てて、初回プレイ判別フラグは折る            
        if (onlyOneTimeFlag == true)
        {
            //ゲームスタートフラグを立てて、初回プレイ判別フラグは折る
            director.GetComponent<IceDirector>().start = true;
            Global.isFirst = false;
            Global.SendReplayCount();       //プレイ回数加算
            onlyOneTimeFlag = false;
        }

    }
}
