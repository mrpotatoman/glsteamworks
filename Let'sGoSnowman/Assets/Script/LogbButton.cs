﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogbButton : MonoBehaviour
{
    public GameObject title;

    public void ClickAction()
    {
        title.GetComponent<Title>().isDispLogb = false;
        gameObject.SetActive(false);
    }
}
