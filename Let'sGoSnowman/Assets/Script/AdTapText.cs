﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class AdTapText : MonoBehaviour
{
    public GameObject AdCam;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            AdCam.GetComponent<AdCam>().AdSetNormalTime();
            SceneManager.LoadScene("AdScene");
        }
    }
}
