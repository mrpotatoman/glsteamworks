﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGMSelector : MonoBehaviour
{
    [SerializeField]
    private AudioSource bgm1;
    [SerializeField]
    private AudioSource bgm2;
    [SerializeField]
    private AudioSource bgm3;

    int bgmNum;
    public void BGMSelect()
    {
        bgmNum = Random.Range(1, 4);
        switch(bgmNum)
        {
            case 1:
                bgm1.Play();
                break;
            case 2:
                bgm2.Play();
                break;
            case 3:
                bgm3.Play();
                break;
        }
    }
    public void BGMStop()
    {
        switch(bgmNum)
        {
            case 1:
                bgm1.Stop();
                break;
            case 2:
                bgm2.Stop();
                break;
            case 3:
                bgm3.Stop();
                break;
        }
    }
}
