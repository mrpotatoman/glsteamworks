﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonGenerate : MonoBehaviour
{
    public GameObject retryButton;


    public void RetryButtonGenerate()
    {
        GameObject obj = Instantiate(retryButton);
        obj.transform.localPosition = new Vector3(167f, 390.8f, 0f);
        obj.transform.parent = transform;
    }
}
