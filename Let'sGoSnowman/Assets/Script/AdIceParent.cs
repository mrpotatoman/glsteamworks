﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdIceParent : MonoBehaviour
{
    public float AdYaxis = -7f;
    private GameObject cam;
    private GameObject corn;
    private bool OnlyFirstTime = true;
    public bool AdisColli = false;
    private bool onlyOneTimeColli = true;

    private GameObject[] AdEffect = new GameObject[6];
    private int random = 0;
    private int random2 = 1;

    [Header("ここにチェックを入れるとめっちゃ祝ってくれる")]
    public bool effectParty = false;        //こいつはプレハブなので探すとき注意
    [Header("何倍して傾かせるか")]
    public float mag = 15f;

    void Start()
    {
        cam = GameObject.Find("MainCamera");
        corn = GameObject.Find("icecreamcorn");
        AdEffect[0] = GameObject.Find("Effect1");
        AdEffect[1] = GameObject.Find("Effect2");
        AdEffect[2] = GameObject.Find("Effect3");
        AdEffect[3] = GameObject.Find("Effect4");
        AdEffect[4] = GameObject.Find("Effect5");
        AdEffect[5] = GameObject.Find("Effect6");
    }


    void Update()
    {
        //アイス落下の文
        if (AdisColli == false)
        {
            //こんくらい動いてほしい(Yだけ変数)
            this.transform.Translate(0.0f, AdYaxis * Time.deltaTime, 0.0f);

            if (cam.GetComponent<AdCam>().AdStart == false)
            {
                Destroy(gameObject);
            }
        }
        else if (AdisColli == true)
        {
            AdYaxis = 0f;
        }        
    }

    void OnCollisionEnter(Collision other)
    {
        if (onlyOneTimeColli == true)
        {
            //カメラに付けた効果音を再生
            cam.GetComponent<AudioSource>().Play();

            if (effectParty == true)
            {
                Effect();
            }

            //corn.GetComponent<AdCornChild>().Adangle -= (transform.position.x - corn.transform.position.x) * mag;

            //接触しているかを知らせる
            AdisColli = true;

            if (OnlyFirstTime == false)
            {
                //もし最高到達点を更新していたらカメラの移動目標値を変更
                if (transform.position.y - 0.3f > cam.transform.position.y)
                {
                    cam.GetComponent<AdCam>().AdtargetPosition = transform.position + new Vector3(-transform.position.x, -0.3f, -17.53f);
                }
            }
            else
            {
                cam.GetComponent<AdCam>().AdtargetPosition = transform.position + new Vector3(-transform.position.x, -0.3f, -17.53f);

                OnlyFirstTime = false;
            }

            //アイスが当たったやつの子供になる(厳密にはそいつを親だと認識する)
            transform.parent = other.gameObject.transform;

            onlyOneTimeColli = false;
        }
    }

    //ランダムでエフェクト再生(effectPartyにチェックが入っている場合)
    private void Effect()
    {
        random = Random.Range(0, 6);
        random2 = Random.Range(0, 6);
        if (random == random2)
        {
            if (random2 < 5)
            {
                random2 += 1;
            }
            else
            {
                random2 -= 1;
            }
        }
        AdEffect[random].GetComponent<ParticleSystem>().Play();
        AdEffect[random2].GetComponent<ParticleSystem>().Play();
    }
}
