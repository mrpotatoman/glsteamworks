﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FinishScript : MonoBehaviour
{
    //Finishにアタッチされていて、間をおいてリザルトを表示するためのスクリプトです


    public bool readyDisp = false;      //表示してもいいよフラグ(コーンで状態を変える)
    private float delta = 0f;           //時間計測

    public GameObject retrybutton;
    public Button retrybutton2;
    public GameObject scoreText;
    public GameObject resScore;
    public GameObject retryText;
    public GameObject yourbest;
    public GameObject rewerdedBotton;
    public GameObject getScore;
    public GameObject greenLeft;
    public GameObject greenRight;
    public GameObject RedLeft;
    public GameObject RedRight;
    public GameObject faceLeft;
    public GameObject faceRight;
    public GameObject cryLeft;
    public GameObject cryRight;
    public GameObject iceImageLeft;
    public GameObject iceImageRight;

    public bool skip = false;          //スキップ用
    private bool oneTime = true;        //同じ処理を繰り返さないため

    void Update()
    {
        //表示しても良いなら
        if(readyDisp == true)
        {
            //マウスクリックでスキップ
            if(Input.GetMouseButtonDown(0))
            {
                skip = true;
            }

            //まだクリックされていない状態で時間が上限に達していない場合
            if (skip != true && delta < 1.5f)
            {
                delta += Time.deltaTime;
            }
            
            //スキップされていない状態で時間を超えた場合
            if(skip != true && delta > 1.5f)
            {
                skip = true;                
            }

            //スキップ状態なら画像表示したり…いろいろ
            if(skip == true)
            {
                if (oneTime == true)
                {
                    resScore.GetComponent<Text>().text = "<size=230>" + getScore.GetComponent<ScoreScript>().scoreHowManyIce + "</size> Ice!";
                    yourbest.GetComponent<Text>().text = "<i>YOUR BEST</i>\n<size=140>" + getScore.GetComponent<ScoreScript>().bestScore + "</size><i>Ice</i>";
                    greenLeft.gameObject.SetActive(false);                                      //傾きUI非アクティブ化
                    RedLeft.gameObject.SetActive(false);
                    faceLeft.gameObject.SetActive(false);
                    cryLeft.gameObject.SetActive(false);
                    greenRight.gameObject.SetActive(false);
                    RedRight.gameObject.SetActive(false);
                    faceRight.gameObject.SetActive(false);
                    cryRight.gameObject.SetActive(false);
                    iceImageLeft.gameObject.SetActive(false);
                    iceImageRight.gameObject.SetActive(false);
                    GetComponent<Text>().color -= new Color(0f, 0f, 0f, 255f);                  //Finishを非表示
                    retrybutton.gameObject.SetActive(true);                                     //リザルトUI系アクティブ化
                    retrybutton2.enabled = true;                                                //テキストを押してもリトライできるようにするため
                    retryText.GetComponent<Text>().color += new Color(0f, 0f, 0f, 255f);
                    retryText.GetComponent<RetryText>().isDisp = true;                          //リトライが表示されている間だけリトライ受付
                    readyDisp = false;                                                          //以下、ループしないための処理
                    oneTime = false;
                }
            }
        }
    }
}
