﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGObjGenerator : MonoBehaviour
{
    //BirdGeneratorに重複してアタッチしています
    //飛行機、気球、天使を生成しています
    //それぞれ、カメラが発生エリアに近づいた時のみ生成を開始します
    //生成間隔はランダムで抽選されます
    //変数はプレハブごとに分けています
    
    
    //生成するプレハブたち
    public GameObject airPlanePrefab;
    public GameObject airBalloonPrefab;
    public GameObject angelPrefab;

    //プレハブごとのランダム生成間隔の下限と上限
    public float lowerLimitAp = 2f;
    public float upperLimitAp = 7f;
    public float lowerLimitAb = 8f;
    public float upperLimitAb = 16f;
    public float lowerLimitAn = 8f;
    public float upperLimitAn = 16f;

    //プレハブごとのランダム生成間隔(結果が代入される)
    private float intervalAp;
    private float intervalAb;
    private float intervalAn;

    //プレハブごとの時間を入れる箱
    private float deltaAp = 0;
    private float deltaAb = 0;
    private float deltaAn = 0;

    [SerializeField]
    private GameObject director;
    [SerializeField]
    private GameObject mainCamera;

    void Start()
    {
        //ランダム生成間隔の抽選
        intervalAp = RandomTimeAp();
        intervalAb = RandomTimeAb();
        intervalAn = RandomTimeAn();
    }

    
    void Update()
    {
        //ゲーム開始後から生成される
        if (director.GetComponent<IceDirector>().start == true)
        {
            //カメラが生成範囲内なら(飛行機編)
            if (mainCamera.transform.position.y < 84f && mainCamera.transform.position.y > 39f)
            {
                //箱に時間を入れ始める
                deltaAp += Time.deltaTime;

                if (deltaAp > intervalAp)
                {
                    deltaAp = 0;
                    GameObject product = Instantiate(airPlanePrefab);
                    float y = Random.Range(59.4f, 71.5f);
                    product.transform.position = new Vector3(10, y, 0);
                }
            }


            //気球編
            if (mainCamera.transform.position.y < 75f && mainCamera.transform.position.y > 9f)
            {
                deltaAb += Time.deltaTime;
                if (deltaAb > intervalAb)
                {
                    deltaAb = 0;
                    GameObject product = Instantiate(airBalloonPrefab);
                    float x = Random.Range(-5.5f, 5.5f);
                    float y = mainCamera.transform.position.y - 14f;
                    product.transform.position = new Vector3(x, y, 0);
                }
            }


            //天使編
            if (mainCamera.transform.position.y > 765f && mainCamera.transform.position.y < 1344f)
            {
                deltaAn += Time.deltaTime;
                if (deltaAn > intervalAn)
                {
                    deltaAn = 0;
                    GameObject product = Instantiate(angelPrefab);
                    float x = Random.Range(-4f, 4f);
                    float y = Random.Range(786f, 1321f);
                    product.transform.position = new Vector3(x, y, 0);
                }
            }
        }
    }

    //ランダム抽選メソッド
    private float RandomTimeAp()
    {
        return Random.Range(lowerLimitAp, upperLimitAp);
    }

    private float RandomTimeAb()
    {
        return Random.Range(lowerLimitAb, upperLimitAb);
    }
    private float RandomTimeAn()
    {
        return Random.Range(lowerLimitAn, upperLimitAn);
    }
}
