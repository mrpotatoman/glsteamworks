﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RetryText : MonoBehaviour
{
    //ゲームオーバーからリトライするためのスクリプト

    public bool gameover = false;

    public bool isHighScore = false;

    public GameObject scoreText;
    public GameObject safeArea;
    public GameObject tomorrowText;
    public GameObject tipsText;
    public GameObject tipsText2;
    public GameObject tipsText3;

    public bool isDisp = false;

    private bool one = true;
    private bool oneTimeButton = true;
    private bool oneTimeTips = true;

    private float alphaSin;
    public GameObject blinkText;
    private float alphaSin2;
    public GameObject blinkText2;
    private IEnumerator SetCoroutine = null;
    private bool isStartCoroutine = false;
    public GameObject newRecordText;

    void Update()
    {
        if (gameover == true)
        {
            //リトライが表示されている間だけ
            if (isDisp == true)
            {
                //ハイスコア取った時用
                float t = 1.0f;
                float f = 5.0f / t;
                alphaSin = Mathf.Sin(2 * Mathf.PI * f * Time.time) / 2 + 0.5f;
                float f2 = 3.0f / t;
                alphaSin2 = Mathf.Sin(2 * Mathf.PI * f2 * Time.time) / 2 + 0.5f;
                SetCoroutine = ColorCoroutine();

                if (one == true)
                {
                    if (Global.isFirstResult == false)
                    {
                        int value = scoreText.GetComponent<ScoreScript>().bestScore - scoreText.GetComponent<ScoreScript>().scoreHowManyIce;
                        if (value <= 100 && value != 0)
                        {
                            if (oneTimeButton == true)
                            {
                                Debug.Log("つづきからできるよ！");
                                safeArea.GetComponent<ButtonGenerate>().RetryButtonGenerate();
                                oneTimeButton = false;
                            }
                        }
                        else if (value == 0 || value >= 101)
                        {
                            if (oneTimeTips == true)
                            {
                                Debug.Log("ヒントを教えるよ！");
                                RandomTips();
                                oneTimeTips = false;
                            }
                        }
                    }

                    if (Global.isFirstResult == true)
                    {
                        if (Global.newLoginBonus == 1 || Global.newLoginBonus == 4 || Global.newLoginBonus == 6)
                        {
                            tomorrowText.gameObject.SetActive(true);
                            Global.isFirstResult = false;
                        }
                        else
                        {
                            Global.isFirstResult = false;
                        }
                    }

                    //ハイスコア更新時はリザルト表示のタイミングでカスタムイベント送信
                    if (isHighScore == true)
                    {
                        newRecordText.gameObject.SetActive(true);
                        SendHighScore(scoreText.GetComponent<ScoreScript>().bestScore);
                        StartCoroutine(SetCoroutine);
                        isStartCoroutine = true;
                        isHighScore = false;
                    }
                    one = false;
                }
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    return;
                }
                //スマホなどのタッチの場合の判定
                if (Input.touchCount > 0)
                {
                    if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                    {
                        return;
                    }
                }

                //マウス左クリックで動画広告再生、その後リトライ
                if (Input.GetMouseButtonDown(0))
                {
                    TouchAction();
                }
            }
        }
    }


    public void TouchAction()
    {
        tomorrowText.gameObject.SetActive(false);
        tipsText.gameObject.SetActive(false);
        tipsText2.gameObject.SetActive(false);
        tipsText3.gameObject.SetActive(false);
        if (isStartCoroutine == true)
        {
            StopCoroutine(SetCoroutine);
            newRecordText.gameObject.SetActive(false);
        }
        ShowAdAndLoadScene();
    }

    public void TouchActionAnother()
    {
        if (isDisp == true)
        {
            tomorrowText.gameObject.SetActive(false);
            tipsText.gameObject.SetActive(false);
            tipsText2.gameObject.SetActive(false);
            tipsText3.gameObject.SetActive(false);
            if (isStartCoroutine == true)
            {
                StopCoroutine(SetCoroutine);
                newRecordText.gameObject.SetActive(false);
            }
            ShowAdAndLoadScene();
        }
    }

    //プレイ後に表示される動画広告
    void ShowAdAndLoadScene()
    {
        gameover = false;
        isDisp = false;
        ScoreScript.isDoubleScore = false;
        // ◆ShowRewardedVideoの結果を受け取るコールバックを設定
        System.Action onFinished = () =>
        {
            // ◆動画リワード広告が最後まで視聴された際の処理
            SceneManager.LoadScene("PlayScene");
        };
        System.Action onSkipped = () =>
        {
            // ◆動画リワード広告がスキップされた際の処理
            SceneManager.LoadScene("PlayScene");
        };
        System.Action onFailed = () =>
        {
            // ◆動画リワード広告が再生失敗した際の処理
            SceneManager.LoadScene("PlayScene");
        };
        // ◆0番の動画リワード広告を表示
        GLS.Ad.ShowRewardedVideo(0, onFinished, onSkipped, onFailed);
    }


    void RandomTips()
    {
        int value = Random.Range(1, 101);
        if (value < 61)
        {
            tipsText3.gameObject.SetActive(true);
        }
        else if (value < 81 && value > 60)
        {
            tipsText2.gameObject.SetActive(true);
        }
        else if (value > 80)
        {
            tipsText.gameObject.SetActive(true);
        }
    }

    //テキスト点滅コルーチン
    IEnumerator ColorCoroutine()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();

            Color _color = blinkText.GetComponent<Text>().color;
            Color _color2 = blinkText2.GetComponent<Text>().color;

            _color.a = alphaSin;
            _color2.a = alphaSin2;

            blinkText.GetComponent<Text>().color = _color;
            blinkText2.GetComponent<Text>().color = _color2;
        }
    }

    //ハイスコア送信時の条件文
    void SendHighScore(int value)
    {
        if (value < 51)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "-50", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 50 && value < 101)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "51-100", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 100 && value < 201)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "101-200", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 200 && value < 301)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "201-300", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 300 && value < 401)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "301-400", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 400 && value < 501)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "401-500", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 500 && value < 601)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "501-600", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 600 && value < 701)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "601-700", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 700 && value < 801)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "701-800", scoreText.GetComponent<ScoreScript>().bestScore);
        }
        else if (value > 800 && value < 901)
        {
            GLS.GLSAnalyticsUtility.TrackEvent("RefineHighScoreReport", "801-900", scoreText.GetComponent<ScoreScript>().bestScore);
        }
    }
}
