﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAction : MonoBehaviour
{
    //このスクリプトは"PlayScene"の"MainCamera"にアタッチされています

    //このスクリプトで行っていること
    //アイスの生成
    

    public IceManager iceManager = null;
    public Vector3 targetPosition;

    [SerializeField]
    private GameObject director;

    [Header("アイスが生成される高さ")]
    public float normalIceTransY = 19f;
    public float spIceTransY = 19.5f;
    [Header("アイスが生成される範囲の幅にあたる部分")]
    public float xWidthLeft = -4.4f;
    public float xWidthRight = 4.4f;
    [Header("SPアイスが生成される範囲の幅にあたる部分")]
    public float spxWidthLeft = -3.8f;
    public float spxWidthRight = 3.8f;

    [Header("スローの時にtimeScaleに設定する時間の進み具合の値")]
    [SerializeField]
    private float timeScale = 0.1f;
    [Header("スローにしたい時間の長さ")]
    [SerializeField]
    private float slowTime = 1f;
    
    private float elapsedTime = 0f;
    private bool isSlowDown = false;

    private bool onlyOneTime = true;

    void Start()
    {
        targetPosition = transform.position;
    }

    void Update()
    {
        if(targetPosition.y < transform.position.y)
        {
            targetPosition = transform.position;
        }


        //カメラが滑らかに移動する文
        transform.position = (transform.position * 9 + targetPosition) / 10.0f;



        //ゲームが開始されているか
        if (director.GetComponent<IceDirector>().start == true)
        {
            if(onlyOneTime == true)
            {
                Global.RandomIceSelect();
                onlyOneTime = false;
            }
            //監督スクリプトの中の落下フラグがfalseなら
            if (director.GetComponent<IceDirector>().IceCreateTimeFlag == true)
            {
                //まずtrueに変える
                director.GetComponent<IceDirector>().IceCreateTimeFlag = false;

                //確率抽選とアイス生成
                int randomValue = Random.Range(1, 100);
                CheckNum(randomValue);
            }
        }


        if(isSlowDown == true)
        {
            elapsedTime += Time.unscaledDeltaTime;          //スローの影響を受けないunscaledDeltaTimeを使う
            if(elapsedTime >= slowTime)
            {
                SetNormalTime();
            }
        }
    }


    //渡された数字によって生成するアイスを決める
    private void CheckNum(int value)
    {
        if(value > 8)
        {
            GeneratedNormalIce();       //92%で通常アイス
        }
        else
        {
            GeneratedSpIce();           //8%でSPアイス
        }
    }

    //Managerで生成された通常アイスを受け取って移動する
    private void GeneratedNormalIce()
    {
        var ice = iceManager.IceCreate();

        float newx = Random.Range(xWidthLeft, xWidthRight);
        float newy = transform.position.y + normalIceTransY;
        //作ったオブジェクトをこの場所に移動させる
        ice.transform.position = new Vector3(newx, newy, 0f);
    }

    //Managerで生成されたSPアイスを受け取って移動する
    private void GeneratedSpIce()
    {
        var ice = iceManager.SpIceCreate();

        float newx = Random.Range(spxWidthLeft, spxWidthRight);
        float newy = transform.position.y + spIceTransY;
        //作ったオブジェクトをこの場所に移動させる
        ice.transform.position = new Vector3(newx, newy, 0f);
    }


    //スローモーション用
    public void SlowDown()
    {
        elapsedTime = 0f;
        Time.timeScale = timeScale;
        isSlowDown = true;
    }

    //通常速度に戻す用
    public void SetNormalTime()
    {
        Time.timeScale = 1f;
        isSlowDown = false;
    }
}
