﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirPlaneController : MonoBehaviour
{
    private float airPlaneSpeed = 2f;
    
    void Update()
    {
        //z軸で回転しているので、左に動かしたい時はYをプラスする
        transform.Translate(0f, airPlaneSpeed * Time.deltaTime, 0f);
        
        //こっちはワールド座標っぽい
        if (transform.position.x < -10)
        {
            Destroy(gameObject);
        }
    }
}
