﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayScore : MonoBehaviour
{
    //プレイ中のスコアの表示

    public GameObject score;
    public GameObject fin;
    

    void Update()
    {
        //プレイが開始されたらスコアを表示する
        if(fin.GetComponent<FinishScript>().readyDisp == true)
        {
            GetComponent<Text>().text = "<size=230>" + score.GetComponent<ScoreScript>().scoreHowManyIce + "</size> Ice!";

            GetComponent<Text>().text = "<i>YOUR BEST</i>\n<size=140>" + score.GetComponent<ScoreScript>().bestScore + "</size> <i>Ice</i>";
        }
    }
}
