﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    //このスクリプトは"PlayScene"の"ResultScoreText"にアタッチされています

    //このスクリプトではスコア計算、表示、別シーンへ送る準備をしています


    public int scoreHowManyIce = 0;     //現在のスコア
    public int bestScore;               //ハイスコア用変数
    public string key = "BEST SCORE";  //ハイスコアの保存先キー

    public Text bestScoreText;  //ハイスコアを表示するテキスト

    private bool oneTime = true;

    public GameObject director;
    //紙吹雪エフェクトたち
    public GameObject pEffect1;
    public GameObject pEffect2;
    public GameObject pEffect3;
    public GameObject pEffect4;
    public GameObject pEffect5;
    public GameObject pEffect6;
    public GameObject retryText;
    public bool oneTimeEffect = false;

    public static bool isDoubleScore = false;


    void Start()
    {
        oneTimeEffect = true;

        //保存しておいたハイスコアをキーで呼び出し取得し保存されていなければ0になる
        bestScore = PlayerPrefs.GetInt(key, 0);

        //ハイスコアを表示(小数点以下第一位まで)
        bestScoreText.text = "YOUR BEST: " + bestScore + "Ice";

        GetComponent<Text>().text = "YOUR BEST: " + bestScore + "Ice";

        //ハイスコアリセット用、一度プレイすると次回リセットされる
        //PlayerPrefs.DeleteAll();
    }


    void Update()
    {
        if (director.GetComponent<IceDirector>().start == true)
        {
            //テキストに反映させる
            GetComponent<Text>().text = "Get: " + scoreHowManyIce + " Ice";

            if (oneTime == true)
            {
                bestScoreText.gameObject.SetActive(true);
                oneTime = false;
            }
        }

        //ハイスコアより現在スコアが高い時
        if (scoreHowManyIce > bestScore)
        {
            //ハイスコア更新
            bestScore = scoreHowManyIce;

            //ハイスコアを保存
            PlayerPrefs.SetInt(key, bestScore);

            retryText.GetComponent<RetryText>().isHighScore = true;

            //エフェクトを再生
            if (oneTimeEffect == true)
            {
                for (int i = 0; i < 5; i++)
                {
                    pEffect1.GetComponent<ParticleSystem>().Play();
                    pEffect2.GetComponent<ParticleSystem>().Play();
                    pEffect3.GetComponent<ParticleSystem>().Play();
                    pEffect4.GetComponent<ParticleSystem>().Play();
                    pEffect5.GetComponent<ParticleSystem>().Play();
                    pEffect6.GetComponent<ParticleSystem>().Play();
                }
                oneTimeEffect = false;
            }
        }
    }
}
