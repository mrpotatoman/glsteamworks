﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RetryBotton : MonoBehaviour
{
    //リワード用
    public void ShowRewardedVideo()
    {
        // ◆ShowRewardedVideoの結果を受け取るコールバックを設定
        System.Action onFinished = () =>
        {
            // ◆動画リワード広告が最後まで視聴された際の処理
            //次はスコア2倍
            ScoreScript.isDoubleScore = true;
            Debug.Log("RewardedVideo:Finish");
            SceneManager.LoadScene("PlayScene");
        };
        System.Action onSkipped = () =>
        {
            // ◆動画リワード広告がスキップされた際の処理
            Debug.Log("RewardedVideo:Skip");
        };
        System.Action onFailed = () =>
        {
            // ◆動画リワード広告が再生失敗した際の処理
            Debug.Log("RewardedVideo:Failed");
        };
        // ◆1番の動画リワード広告を表示
        GLS.Ad.ShowRewardedVideo(1, onFinished, onSkipped, onFailed);
    }
}
