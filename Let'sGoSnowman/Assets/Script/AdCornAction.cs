﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdCornAction : MonoBehaviour
{
    GameObject mainCamera;
    public float cornMoveSpeed = 0.5f;

    void Start()
    {
        mainCamera = GameObject.Find("MainCamera");
    }

    void Update()
    {
        //入力に合わせてコーンが移動する文

        if (Input.GetMouseButton(0))  //画面を押してる間
        {
            Vector3 screenPos = Input.mousePosition;    //この座標はディスプレイ上の座標(今回は横が1080px)

            // zの値をスクリーンの位置(カメラからz離れた位置)
            screenPos.z = Mathf.Abs(mainCamera.transform.position.z);

            // スクリーン座標をワールド座標に変換
            Vector3 touchPos = Camera.main.ScreenToWorldPoint(screenPos);

            //現在位置とタッチされた位置までの距離の数分の1を求める(0.5なら50%)
            float movePosX = Mathf.Lerp(transform.position.x, touchPos.x, cornMoveSpeed);

            //数分の1ずつ移動させることで滑らかな動きになる
            transform.position = new Vector3(movePosX, 0f, touchPos.z);
        }
    }
}
