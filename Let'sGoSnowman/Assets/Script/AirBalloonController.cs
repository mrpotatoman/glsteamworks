﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirBalloonController : MonoBehaviour
{

    private GameObject mainCamera;

    void Start()
    {
        mainCamera = GameObject.Find("MainCamera");
    }

    
    void Update()
    {
        Vector3 pos1 = transform.position;
        Vector3 pos2 = mainCamera.transform.position;

        Vector3 d = pos2 - pos1;
        float dm = d.magnitude;

        //カメラと近ければ
        if(dm < 30f)
        {
            transform.Translate(0f, 2.5f * Time.deltaTime, 0f);     //移動
        }
        //遠ければ
        else
        {
            Destroy(gameObject);        //破壊
        }
    }
}
