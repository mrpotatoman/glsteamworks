﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RecoveryIce : MonoBehaviour
{
    private GameObject cam;
    private GameObject resScoreText;
    private GameObject resText;
    private GameObject corn;

    //エフェクト
    public GameObject rEffectLight;
    public GameObject rEffectSpark;
    public GameObject rEffectHeart;
    private bool one = true;

    private bool spIsColli = false;

    private bool onlyOneTimeColli = true;

    [Header("こいつの落下速度")]
    public float spRecYaxis = -7f;


    void Start()
    {
        cam = GameObject.Find("MainCamera");
        resScoreText = GameObject.Find("ResultScoreText");
        resText = GameObject.Find("ResultText");
        corn = GameObject.Find("icecreamcorn");
    }

    void Update()
    {
        //アイス落下の文
        if (spIsColli == false)
        {
            //こんくらい動いてほしい(Yだけ変数)
            this.transform.Translate(0.0f, spRecYaxis * Time.deltaTime, 0.0f);
            if(one == true)
            {
                rEffectLight.GetComponent<ParticleSystem>().Play();         //落下中はエフェクト再生
                rEffectSpark.GetComponent<ParticleSystem>().Play();
            }

            if (resText.GetComponent<RetryText>().gameover == true)         //ゲームオーバー時
            {
                rEffectLight.GetComponent<ParticleSystem>().Stop();
                rEffectSpark.GetComponent<ParticleSystem>().Stop();
                Destroy(gameObject);                
            }
        }
        else if (spIsColli == true)
        {
            spRecYaxis = 0f;
            rEffectLight.GetComponent<ParticleSystem>().Stop();             //接触時はエフェクト停止
            rEffectSpark.GetComponent<ParticleSystem>().Stop();
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (onlyOneTimeColli == true)
        {
            //カメラに付けた効果音を再生
            cam.GetComponent<AudioSource>().Play();

            //エフェクト再生
            rEffectHeart.GetComponent<ParticleSystem>().Play();

            //スコア加算
            resScoreText.GetComponent<ScoreScript>().scoreHowManyIce += 30;         //めっちゃスコアあげちゃう

            //コーンの傾きをリセットする
            corn.GetComponent<CornAction>().angle = 0f;

            //接触しているかを知らせる
            spIsColli = true;

            //もし最高到達点を更新していたらカメラの移動目標値を変更
            if (transform.position.y - 0.3f > cam.transform.position.y)
            {
                cam.GetComponent<CameraAction>().targetPosition = transform.position + new Vector3(-transform.position.x, -0.3f, -17.53f);
            }

            //アイスが当たったやつの子供になる(厳密にはそいつを親だと認識する)
            transform.parent = other.gameObject.transform;

            onlyOneTimeColli = false;
        }
    }
}
