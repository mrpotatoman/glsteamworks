﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXCheck : MonoBehaviour
{
    public GameObject effect;
    public void PressDown ()
    {
        effect.GetComponent<ParticleSystem>().Play();
    }
}
